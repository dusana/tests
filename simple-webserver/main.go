package main

import (
	"html/template"
	"net/http"
	"time"
)

func main() {
	// Define template directory
	templates := template.Must(template.ParseFiles("layouts/base.html", "templates/index.html", "templates/weather.html"))

	// Handler functions for different routes
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		currentTime := time.Now().Format("Monday, January 2nd, 2006 - 03:04 PM") // Adjust format as needed
		err := templates.ExecuteTemplate(w, "index.html", currentTime)           // Pass current time as data
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})

	http.HandleFunc("/weather/", func(w http.ResponseWriter, r *http.Request) {
		err := templates.ExecuteTemplate(w, "weather.html", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})

	// Start the server
	http.ListenAndServe(":8080", nil)
}
